﻿using Microsoft.AspNetCore.Mvc;
using QLBT1.DataDB;
using QLBT1.Models;

namespace QLBT1.Controllers
{
    public class ThanhPhanThuocController : Controller
    {
        public IActionResult ThanhPhanThuoc(string tennguoibenh , string quequan , int tuoi , int cannang)
        {
            if(tennguoibenh != null)
            {
                HttpContext.Session.SetString("tennguoibenh", tennguoibenh);
                HttpContext.Session.SetString("quequan", quequan);
                HttpContext.Session.SetString("tuoi", tuoi.ToString());
                HttpContext.Session.SetString("cannang", cannang.ToString());
            }
            return View();
        }

        public BaseReponsitory CheckThanhPhanThuoc(string thanhphanthuoc)
        {
            var _context = new QLBTContext();
            var data = _context.ThanhPhans.Where(x => x.TenThanhPhan == thanhphanthuoc).FirstOrDefault();
            if (data == null)
            {
                return new BaseReponsitory()
                {
                    Status = false,
                    Message = "Thành Phần Thuốc Không Tồn Tại"
                };
            }
            else
            {
                return new BaseReponsitory()
                {
                    Status = true,
                    Message = ""
                };
            }

        }

    }
}
