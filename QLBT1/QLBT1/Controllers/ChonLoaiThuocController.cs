﻿using Microsoft.AspNetCore.Mvc;
using QLBT1.DataDB;
using QLBT1.Models;

namespace QLBT1.Controllers
{
    public class ChonLoaiThuocController : Controller
    {
        QLBTContext Db = new QLBTContext();
        public ActionResult ChonLoaiThuoc(string ThanhPhanThuoc)
        {
            var data = (from thanhphan in Db.ThanhPhans
                     join thuoc in Db.Thuocs on thanhphan.Id equals thuoc.IdThanhPhan
                     where thanhphan.TenThanhPhan == ThanhPhanThuoc
                     select new thuoc()
                     {
                         IdThuoc = thuoc.Id,
                         Id = thanhphan.Id,
                         TenThuoc = thuoc.TenThuoc,
                         CachDung = thanhphan.CachDung,
                         ChiDinh = thanhphan.ChiDinh,
                     }).ToList();
            
            return View(data);
        }
        [HttpPost]
        public ActionResult DonThuoc(string thuoc , string quay , string thanh)
        {
            ViewBag.TenBenhNhan = HttpContext.Session.GetString("tennguoibenh");
            ViewBag.QueQuan = HttpContext.Session.GetString("quequan");
            ViewBag.Tuoi = HttpContext.Session.GetString("tuoi");
            ViewBag.CanNang = HttpContext.Session.GetString("cannang");

            ViewBag.tg = DateTime.Now;
            ViewBag.quay = quay;

            //var CT = Db.Thuocs.FirstOrDefault(x => x.TenThuoc == thuoc);
            //var Tp = Db.ThanhPhans.Where(x => x.Id == CT.IdThanhPhan).ToList();
            //var th = Db.Thuocs.Where(x => x.TenThuoc == thuoc).ToList();
            //var DL = Db.LieuDungs.Where(x => x.IdThuoc == CT.Id).ToList();

            //var b = (from clt in Tp
            //           join PT in th on clt.Id equals PT.IdThanhPhan 
            //           join LD in DL on  PT.Id equals LD.IdThuoc into pss
            //         from LD in pss.DefaultIfEmpty()
            //         select new DT()
            //         {
            //             Sl = LD.Sl,
            //             IdThuoc = clt.Id = PT.Id,
            //             Id = clt.Id,
            //             TenThuoc = PT.TenThuoc,
            //             CachDung = clt.CachDung,
            //             ChiDinh = clt.ChiDinh,
            //         }).ToList();
            //var data = b;


            var data = (from thanhphan in Db.ThanhPhans
                        join thuocs in Db.Thuocs on thanhphan.Id equals thuocs.IdThanhPhan
                        join lieudung in Db.LieuDungs on thuocs.Id equals lieudung.IdThuoc
                        where thuocs.TenThuoc == thuoc
                        select new DT()
                        {
                            Sl = lieudung.Sl,
                            IdThuoc = thuocs.Id,
                            Id = thanhphan.Id,
                            TenThuoc = thuocs.TenThuoc,
                            CachDung = thanhphan.CachDung,
                            ChiDinh = thanhphan.ChiDinh,
                        }).ToList();

            return View(data);
        }
        public ActionResult Tacgia()
        {
            return View();
        }
        public ActionResult HoTro()
        {
            return View();
        }

    }
}
