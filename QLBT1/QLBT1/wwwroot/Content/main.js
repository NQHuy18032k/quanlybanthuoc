﻿var lst = [];
var lstsv = [];

// danh sách học sinh
var lst_sv = [
    {
        "id": 1,
        "MaSinhVien": "123ABC4563",
        "HoTen": "Nguyễn Thành Luân",
        "NgaySinh": "2000-04-29",
        "SoDienThoai": "0377263583",
        "DiaChi": "Số 19 ngõ 110",
        "TinhTP": {
            "value": 1,
            "Name": "Hà Nội"
        },
        "QuanHuyen": {
            "value": 1,
            "Name": "Cầu Giấy"
        },
        "Phuong": {
            "value": 1,
            "Name": "Trung Hòa"
        },
        "GioiTinh": {
            "id": "nam",
            "Name": "Nam"
        },
        "CanNang": 5,
        "SoThich": [
            { "id": "nghenhac", "Name": "Nghe Nhạc" },
            { "id": "thethao", "Name": "Đá Bóng" }
        ],
        "GhiChu": "abcd"

    },
    {
        "id": 2,
        "MaSinhVien": "123ABC4564",
        "HoTen": "Nguyễn Văn A",
        "NgaySinh": "2000-04-29",
        "SoDienThoai": "0377263583",
        "DiaChi": "Số 29 ngõ 110",
        "TinhTP": {
            "value": 1,
            "Name": "Hà Nội"
        },
        "QuanHuyen": {
            "value": 1,
            "Name": "Cầu Giấy"
        },
        "Phuong": {
            "value": 1,
            "Name": "Trung Hòa"
        },
        "GioiTinh": {
            "id": "nam",
            "Name": "Nam"
        },
        "CanNang": 5,
        "SoThich": [
            { "id": "thethao", "Name": "Đá Bóng" }
        ],
        "GhiChu": "abcd"
    },
    {
        "id": 3,
        "MaSinhVien": "123ABC4565",
        "HoTen": "Nguyễn Thị B",
        "NgaySinh": "2000-04-29",
        "SoDienThoai": "0377263583",
        "DiaChi": "Số 39 ngõ 110",
        "TinhTP": {
            "value": 1,
            "Name": "Hà Nội"
        },
        "QuanHuyen": {
            "value": 1,
            "Name": "Cầu Giấy"
        },
        "Phuong": {
            "value": 1,
            "Name": "Trung Hòa"
        },
        "GioiTinh": {
            "id": "nam",
            "Name": "Nam"
        },
        "CanNang": 5,
        "SoThich": [
            { "id": "nghenhac", "Name": "Nghe Nhạc" },
        ],
        "GhiChu": "abcd"
    },
    {
        "id": 4,
        "MaSinhVien": "123ABC4568",
        "HoTen": "Nguyễn Văn C",
        "NgaySinh": "2000-04-29",
        "SoDienThoai": "0377263583",
        "DiaChi": "Số 119 ngõ 110",
        "TinhTP": {
            "value": 1,
            "Name": "Hà Nội"
        },
        "QuanHuyen": {
            "value": 1,
            "Name": "Cầu Giấy"
        },
        "Phuong": {
            "value": 1,
            "Name": "Trung Hòa"
        },
        "GioiTinh": {
            "id": "nam",
            "Name": "Nam"
        },
        "CanNang": 5,
        "SoThich": [
            { "id": "thethao", "Name": "Đá Bóng" }
        ],
        "GhiChu": "abcd"
    },
    {
        "id": 5,
        "MaSinhVien": "123ABC4567",
        "HoTen": "Nguyễn Văn D",
        "NgaySinh": "2000-04-29",
        "SoDienThoai": "0377263583",
        "DiaChi": "Số 129 ngõ 110",
        "TinhTP": {
            "value": 1,
            "Name": "Hà Nội"
        },
        "QuanHuyen": {
            "value": 1,
            "Name": "Cầu Giấy"
        },
        "Phuong": {
            "value": 1,
            "Name": "Trung Hòa"
        },
        "GioiTinh": {
            "id": "nam",
            "Name": "Nam"
        },
        "CanNang": 5,
        "SoThich": [
            { "id": "nghenhac", "Name": "Nghe Nhạc" },
        ],
        "GhiChu": "abcd"
    }
]
//danh sách các xã huyện tỉnh để lấy ra
var lst_country = [
    {
        "Id": 1,
        "Name": "Hà Nội",
        "QuanHuyen": [
            {
                "Id": 1,
                "Name": "Cầu Giấy",
                "IdTinh": 1,
                "XaPhuong": [
                    {
                        "Id": 1,
                        "Name": "Dịch Vọng Hậu",
                        "idhuyen": 1
                    },
                    {
                        "Id": 2,
                        "Name": "Trung Hòa",
                        "idhuyen": 1
                    },
                    {
                        "Id": 3,
                        "Name": " Thụy Khuê",
                        "idhuyen": 1
                    },
                    {
                        "Id": 4,
                        "Name": "Nhật Tân",
                        "idhuyen": 1
                    },
                ]
            },
            {

                "Id": 2,
                "IdTinh": 1,
                "Name": "Nam Từ Liêm",
                "XaPhuong": [{
                    "Id": 5,
                    "Name": "Cầu Diễn",
                    "idhuyen": 2
                },
                {
                    "Id": 6,
                    "Name": " Mỹ Đình 2",
                    "idhuyen": 2
                }
                ]
            }
        ]
    }
]


//mở popup thêm mới học sinh
$('#btnThemMoi').click(function () {
    $("#exampleModal").load("/Views/Popup/PopupCreateUpdate.html", function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {
            $('#header-title').text('Thêm Mới Sinh Viên')
            GetTinhTp();
            $('#exampleModal').modal('show');
        }
        if (statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });

})
//load dữ liệu tỉnh , thành phố có sẵn ra
function GetTinhTp() {
    for (i = 0; i < lst_country.length; i++) {
        $('#TinhThanhPho').append('<option class="ListTinh" value="' + lst_country[i].Id + '">' + lst_country[i].Name + '</option>')
    }
}


//mở form sửa sinh viên

function SuaSinhVien(id) {
    $("#exampleModal").load("/Views/Popup/PopupCreateUpdate.html", function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {

            GetTinhTp();
            $('#header-title').text('Sửa Thông Tin Sinh Viên')
            var sv = lst_sv.find(x => x.id == id);

            $('#idSinhVien').val(sv.id);

            $('#MaSinhVien').val(sv.MaSinhVien);

            $('#TenSinhVien').val(sv.HoTen);

            $('#DiaChi').val(sv.DiaChi);

            $('#SoDienThoai').val(sv.SoDienThoai);

            $('#CanNang').val(sv.CanNang);

            $('#GhiChu').val(sv.GhiChu);

            $('#NgaySinh').val(sv.NgaySinh);

            for (i = 0; i < sv.SoThich.length; i++) {
                $("#" + sv.SoThich[i].id).prop("checked", true);
            }

            $('#' + sv.GioiTinh.id).prop('checked', true);

            $('#exampleModal').modal('show');
        }

        if (statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });
}
//checked tất cả

$(document).ready(function () {

    $('#checkall').click(function () {
        if ($('#checkall').prop("checked") == true) {
            $('input:checkbox').prop('checked', true);
            $("#btnXacNhanXoaNhieuSinhVien").removeAttr('disabled');
        }
        else {
            $('input:checkbox').prop('checked', false);
            $("#btnXacNhanXoaNhieuSinhVien").prop('disabled', true);
        }
    });

});

//khi load trang lúc mới vào sẽ load list sinh viên
$(document).ready(function () {

    GetListSv();
});

//lấy ra danh sách sinh viên

function GetListSv() {

    $('tr').remove('.DanhSachSinhVien')
    for (i = 0; i < lst_sv.length; i++) {

        //chuyển date thành dạng dd/MM/yyyy để hiển thị

        var date = new Date(lst_sv[i].NgaySinh)

        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();

        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        $('#DanhSachSinhVien').append(
            '<tr class="DanhSachSinhVien"><td style="text-align: center;"><input type="checkbox" id="' + lst_sv[i].id + '" name="chkitem" onclick="checkitem()"></td>' +
            '<td>' + lst_sv[i].MaSinhVien + '</td>' +
            '<td><strong>' + lst_sv[i].HoTen + '</strong></td>' +
            '<td>' + day + '/' + month + '/' + year + '</td>' +
            '<td>' + lst_sv[i].SoDienThoai + '</td>' +
            '<td>' + lst_sv[i].DiaChi + '</td>' +
            '<td>' + lst_sv[i].GioiTinh.Name + '</td>' +
            '<td onclick ="SuaSinhVien(' + lst_sv[i].id + ')" style="text-align: center;"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color:blue"></i></td>' +
            ' <td onclick ="Xoa(' + lst_sv[i].id + ')"  style="text-align: center;"> <i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>' +
            '</tr>'
        )
    }
}

function checkitem() {

    var checkbox = $("input[name='chkitem']");

    for (var i = 0; i < checkbox.length; i++) {

        if (checkbox[i].checked) {
            $("#btnXacNhanXoaNhieuSinhVien").removeAttr('disabled');
            break;
        }
        else {
            $("#btnXacNhanXoaNhieuSinhVien").prop('disabled', true);
        }
    }
}

    


$('#timkiem').change(function () {


    var tk = $('#timkiem').val().toLowerCase();

    if (lstsv.length <= 0) {

        lst = lst_sv.filter(x => x.HoTen.toLowerCase().includes(tk) || x.SoDienThoai.includes(tk));

        return Lst_search(lst);
    }
    else {
        lst = lstsv.filter(x => x.HoTen.toLowerCase().includes(tk) || x.SoDienThoai.includes(tk));

        return Lst_search(lst);
    }

})

function Lst_search(item) {
    $('tr').remove('.DanhSachSinhVien');
    for (i = 0; i < item.length; i++) {
        $('#DanhSachSinhVien').append(
            '<tr class="DanhSachSinhVien"><td style="text-align: center;"><input type="checkbox" id="chkitem" class="chkitem"></td>' +
            '<td>' + item[i].MaSinhVien + '</td>' +
            '<td><strong>' + item[i].HoTen + '</strong></td>' +
            '<td>' + item[i].NgaySinh + '</td>' +
            '<td>' + item[i].SoDienThoai + '</td>' +
            '<td>' + item[i].DiaChi + '</td>' +
            '<td>' + item[i].GioiTinh.Name + '</td>' +
            '<td onclick ="SuaSinhVien(' + item[i].id + ')"  style="text-align: center;"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color:blue;"></i></td>' +
            ' <td onclick ="XoaSinhVien(' + item[i].id + ')" style="text-align: center;"> <i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>' +
            '</tr>'
        )
    }
}


$('#btnXacNhanXoaNhieuSinhVien').click(function () {

    $("#staticBackdroplist").load("/Views/Popup/PopupMessageDeleteList.html", function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {
            $('#staticBackdroplist').modal('show')
        }
        if (statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });
})

//lọc theo giới tính
$('#GioiTinh').change(function () {

    var GioiTinh = $('#GioiTinh').val();

    if (GioiTinh == 1 || GioiTinh == 0) {
        if (lst.length > 0) {
            return Lst_search(lst);
        }
        else {
            return Lst_search(lst_sv);
        }
    }
    else {
        if (lst.length > 0) {
            return Lst_search(lst.filter(x => x.GioiTinh.Name.includes(GioiTinh)));
        }
        else {
            lstsv = lst_sv.filter(x => x.GioiTinh.Name.includes(GioiTinh));
            return Lst_search(lstsv);
        }
    }
})


// Xóa Từng Sinh Viên Theo Id

//Đẩy id sinh viên vào popup để xóa
function Xoa(id) {
    $("#staticBackdrop").load("/Views/Popup/PopupMessageDelete.html", function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {
            $('#staticBackdrop').modal('show');
            $('#idhs').val(id);
            console.log(121, id)
        }
        if (statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });
}

