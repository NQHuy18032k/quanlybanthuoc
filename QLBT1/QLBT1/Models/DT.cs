﻿namespace QLBT1.Models
{
    public class DT
    {
        public string? TenThuoc { get; set; }
        public int Id { get; set; }
        public string? TenThanhPhan { get; set; }
        public string? CachDung { get; set; }
        public string? ChiDinh { get; set; }
        public int? IdThuoc { get; set; }
       
        public int? Sl { get; set; }
        public int? IdDonVi { get; set; }
        public int? IdNhomTuoi { get; set; }
    }
}
