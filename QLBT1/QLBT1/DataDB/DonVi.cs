﻿using System;
using System.Collections.Generic;

namespace QLBT1.DataDB
{
    public partial class DonVi
    {
        public DonVi()
        {
            LieuDungs = new HashSet<LieuDung>();
        }

        public int Id { get; set; }
        public string? TenDonVi { get; set; }

        public virtual ICollection<LieuDung> LieuDungs { get; set; }
    }
}
