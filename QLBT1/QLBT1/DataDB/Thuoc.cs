﻿using System;
using System.Collections.Generic;

namespace QLBT1.DataDB
{
    public partial class Thuoc
    {
        public Thuoc()
        {
            DonThuocs = new HashSet<DonThuoc>();
            QuayThuocs = new HashSet<QuayThuoc>();
        }

        public int Id { get; set; }
        public string? TenThuoc { get; set; }
        public int? IdThanhPhan { get; set; }

        public virtual ICollection<DonThuoc> DonThuocs { get; set; }
        public virtual ICollection<QuayThuoc> QuayThuocs { get; set; }
    }
}
