﻿using System;
using System.Collections.Generic;

namespace QLBT1.DataDB
{
    public partial class QuayThuoc
    {
        public int Id { get; set; }
        public string? TenQuay { get; set; }
        public int? Sl { get; set; }
        public int? IdThuoc { get; set; }

        public virtual Thuoc? IdThuocNavigation { get; set; }
    }
}
