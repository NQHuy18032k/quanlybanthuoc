﻿using System;
using System.Collections.Generic;

namespace QLBT1.DataDB
{
    public partial class NhomTuoi
    {
        public NhomTuoi()
        {
            LieuDungs = new HashSet<LieuDung>();
        }

        public int Id { get; set; }
        public string? TenNhomTuoi { get; set; }

        public virtual ICollection<LieuDung> LieuDungs { get; set; }
    }
}
