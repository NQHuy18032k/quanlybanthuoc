﻿using System;
using System.Collections.Generic;

namespace QLBT1.DataDB
{
    public partial class DonThuoc
    {
        public int Id { get; set; }
        public int? IdThuoc { get; set; }
        public string? TenKhachHang { get; set; }
        public string? DiaChi { get; set; }
        public string? Tuoi { get; set; }
        public string? CanNang { get; set; }

        public virtual Thuoc? IdThuocNavigation { get; set; }
    }
}
