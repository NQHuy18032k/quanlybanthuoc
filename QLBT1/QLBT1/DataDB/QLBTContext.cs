﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace QLBT1.DataDB
{
    public partial class QLBTContext : DbContext
    {
        public QLBTContext()
        {
        }

        public QLBTContext(DbContextOptions<QLBTContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DonThuoc> DonThuocs { get; set; } = null!;
        public virtual DbSet<DonVi> DonVis { get; set; } = null!;
        public virtual DbSet<LieuDung> LieuDungs { get; set; } = null!;
        public virtual DbSet<NhomTuoi> NhomTuois { get; set; } = null!;
        public virtual DbSet<QuayThuoc> QuayThuocs { get; set; } = null!;
        public virtual DbSet<ThanhPhan> ThanhPhans { get; set; } = null!;
        public virtual DbSet<Thuoc> Thuocs { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=localhost;Database=QLBT;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DonThuoc>(entity =>
            {
                entity.ToTable("DonThuoc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CanNang).HasMaxLength(50);

                entity.Property(e => e.DiaChi).HasMaxLength(50);

                entity.Property(e => e.TenKhachHang).HasMaxLength(80);

                entity.Property(e => e.Tuoi).HasMaxLength(50);

                entity.HasOne(d => d.IdThuocNavigation)
                    .WithMany(p => p.DonThuocs)
                    .HasForeignKey(d => d.IdThuoc)
                    .HasConstraintName("FK_DonThuoc_Thuoc");
            });

            modelBuilder.Entity<DonVi>(entity =>
            {
                entity.ToTable("DonVi");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.TenDonVi).HasMaxLength(50);
            });

            modelBuilder.Entity<LieuDung>(entity =>
            {
                entity.ToTable("LieuDung");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Sl).HasColumnName("SL");

                entity.HasOne(d => d.IdDonViNavigation)
                    .WithMany(p => p.LieuDungs)
                    .HasForeignKey(d => d.IdDonVi)
                    .HasConstraintName("FK_LieuDung_DonVi");

                entity.HasOne(d => d.IdNhomTuoiNavigation)
                    .WithMany(p => p.LieuDungs)
                    .HasForeignKey(d => d.IdNhomTuoi)
                    .HasConstraintName("FK_LieuDung_NhomTuoi");
            });

            modelBuilder.Entity<NhomTuoi>(entity =>
            {
                entity.ToTable("NhomTuoi");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.TenNhomTuoi).HasMaxLength(250);
            });

            modelBuilder.Entity<QuayThuoc>(entity =>
            {
                entity.ToTable("QuayThuoc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdThuoc).HasColumnName("idThuoc");

                entity.Property(e => e.Sl).HasColumnName("SL");

                entity.Property(e => e.TenQuay).HasMaxLength(50);

                entity.HasOne(d => d.IdThuocNavigation)
                    .WithMany(p => p.QuayThuocs)
                    .HasForeignKey(d => d.IdThuoc)
                    .HasConstraintName("FK_QuayThuoc_Thuoc");
            });

            modelBuilder.Entity<ThanhPhan>(entity =>
            {
                entity.ToTable("ThanhPhan");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.TenThanhPhan).HasMaxLength(250);
            });

            modelBuilder.Entity<Thuoc>(entity =>
            {
                entity.ToTable("Thuoc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.TenThuoc).HasMaxLength(250);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
