﻿using System;
using System.Collections.Generic;

namespace QLBT1.DataDB
{
    public partial class LieuDung
    {
        public int Id { get; set; }
        public int? IdThuoc { get; set; }
        public int? Sl { get; set; }
        public int? IdDonVi { get; set; }
        public int? IdNhomTuoi { get; set; }

        public virtual DonVi? IdDonViNavigation { get; set; }
        public virtual NhomTuoi? IdNhomTuoiNavigation { get; set; }
    }
}
