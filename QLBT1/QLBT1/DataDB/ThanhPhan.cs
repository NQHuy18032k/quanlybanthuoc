﻿using System;
using System.Collections.Generic;

namespace QLBT1.DataDB
{
    public partial class ThanhPhan
    {
        public int Id { get; set; }
        public string? TenThanhPhan { get; set; }
        public string? CachDung { get; set; }
        public string? ChiDinh { get; set; }
    }
}
